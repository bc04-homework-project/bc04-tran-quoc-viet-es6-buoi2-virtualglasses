export let renderGlassList = (glassesList) => {
  let contentHTML = "";
  glassesList.forEach((glasses) => {
    let glassesImg = `<img onclick=wearGlasses('${glasses.id}') class="col-4 py-5" src="${glasses.virtualImg}" ></img>`;

    contentHTML += glassesImg;
  });

  document.getElementById("vglassesList").innerHTML = contentHTML;
};

export let renderGlassData = (glasses) => {
  let glassesData = `
    <div>
<p>
    <span>${glasses.name} - ${glasses.brand} (${glasses.color}) </span>
</p>
<p><span class="bg-danger p-2 rounded-lg">$${glasses.price}</span> <span class="text-success">Stocking</span>
</p> 
<p>
    ${glasses.description}
</p>
</div>
    `;
  document.getElementById("glassesInfo").innerHTML = glassesData;
};

export let timKiemViTri = (id, glassesList) => {
  let index = glassesList.findIndex((glasses) => {
    return glasses.id == id;
  });

  return index;
};
