import {
  renderGlassData,
  renderGlassList,
  timKiemViTri,
} from "./controller/controller.js";
import { dataGlasses } from "./data/data.js";

renderGlassList(dataGlasses);
document.getElementById("btnBefore").disabled = true;
document.getElementById("btnAfter").disabled = true;

var idGlasses;

let wearGlasses = (id) => {
  let index = timKiemViTri(id, dataGlasses);
  idGlasses = id;

  document.getElementById(
    "avatar"
  ).innerHTML = `<img src="${dataGlasses[index].virtualImg}" ></img>`;

  renderGlassData(dataGlasses[index]);
  document.getElementById("btnBefore").disabled = false;
  document.getElementById("btnAfter").disabled = false;
};

let removeGlasses = (value) => {
  if (value) {
    wearGlasses(idGlasses);
  } else {
    document.getElementById("avatar").innerHTML = "";
  }
};

window.renderGlassList = renderGlassList;
window.wearGlasses = wearGlasses;
window.timKiemViTri = timKiemViTri;
window.removeGlasses = removeGlasses;
